FROM scratch
LABEL maintainer="roger.pales@vualto.com"

ADD build/ca-certificates.crt /etc/ssl/certs/ca-certificates.crt
ADD backup-routine /
CMD ["/backup-routine"]
