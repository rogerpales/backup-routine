# README #

Backup routine, every X create .tar of whatever

## go build
```
CGO_ENABLED=0 GOOS=linux go build -a -installsuffix cgo -o backup-routine .
```

## docker build
```
docker build -t vualto/backup-routine:0.1.2 .
```

## docker push (docker login)
```
docker push vualto/backup-routine:0.1.2
```

INPUT comma separated files or directories

OUTPUT_LOCATION output directory full path (docker volume)

FREQUENCY may be crontab interval string such as "0 0 0 * * 0" or "hourly", "daily", "weekly", "monthly", "yearly"

FILENAME base backup filename, e.g. mybackup.tar

CLEAN_UP "daily", "weekly", "monthly" or "yearly"

## Run
```
docker run -d --name backup-routine \
-e INPUT=/input/input \
-e OUTPUT_LOCATION=/output/ \
-e FILENAME=my_backup.tar \
-e FREQUENCY=hourly \
-e CLEAN_UP=weekly \
-v /home/ubuntu/backup_routine_test:/input \
-v /home/ubuntu/backup_routine_test/output:/output \
-p 8000:8000 \
vualto/backup-routine:0.1.0
```

running the example above you'd get hourly backups:
```
$ ls /home/ubuntu/backup_routine_test/output
  04.05.18T150405.my_backup.tar
  04.05.18T160000.my_backup.tar
  04.05.18T170000.my_backup.tar
  04.05.18T180000.my_backup.tar
```

every three hours crontab: `"0 */3 * * *"`
