package main

import (
	"errors"
	"fmt"
	"io/ioutil"
	"os"
	"strings"
	"time"

	"github.com/mholt/archiver"
	cron "gopkg.in/robfig/cron.v2"
)

var cutoff time.Duration
var frequency string
var input []string
var outputLocation string

func run() {
	makeBackup()
	deleteOldBackups()
}

func setVariables() (err error) {
	cutoff = getCutoffDuration()
	input = getInput()
	outputLocation = os.Getenv("OUTPUT_LOCATION")

	if outputLocation[len(outputLocation)-1:] != "/" {
		outputLocation += "/"
	}

	frequency, err = getFrequency()
	if err != nil {
		return err
	}

	return nil
}

func checkRequiredEnvVariables() error {
	requiredParams := "INPUT,OUTPUT_LOCATION,FILENAME,FREQUENCY,CLEAN_UP"
	source := strings.Split(requiredParams, ",")
	missing := []string{}

	for i := 0; i < len(source); i++ {
		if os.Getenv(source[i]) == "" {
			missing = append(missing, source[i])
		}
	}

	if len(missing) == 0 {
		return nil
	}

	return fmt.Errorf("Missing environment variables: %v", missing)
}

func getInput() []string {
	inputString := os.Getenv("INPUT")
	return strings.Split(inputString, ",")
}

func isCrontab(str string) bool {
	spaceCount := strings.Count(str, " ")

	if spaceCount == 4 || spaceCount == 5 {
		return true
	}
	return false
}

func getCutoffDuration() time.Duration {
	var days time.Duration

	switch os.Getenv("CLEAN_UP") {
	case "monthly":
		days = 30
	case "weekly":
		days = 7
	case "daily":
		days = 1
	default:
		days = 365
	}

	return days * 24 * time.Hour
}

func getFrequency() (string, error) {
	frequency := os.Getenv("FREQUENCY")

	if isCrontab(frequency) {
		return frequency, nil
	}

	switch frequency {
	case "yearly":
		return "0 0 0 1 1 *", nil
	case "monthly":
		return "0 0 0 1 * *", nil
	case "weekly":
		return "0 0 0 * * 0", nil
	case "daily":
		return "0 0 0 * * *", nil
	case "hourly":
		return "0 0 * * * *", nil
	default:
		return "", errors.New("Invalid frequency")
	}
}

func makeBackup() {
	if outputLocation[len(outputLocation)-1:] != "/" {
		outputLocation += "/"
	}
	output := outputLocation + time.Now().Format("02.01.06T150405") + "." + os.Getenv("FILENAME")

	if err := archiver.Tar.Make(output, input); err != nil {
		fmt.Println(err.Error())
	} else {
		fmt.Println("Backup created: " + output)
	}
}

func deleteOldBackups() {
	fileInfo, err := ioutil.ReadDir(outputLocation)
	if err != nil {
		fmt.Println("Delete old backups error: " + err.Error())
	}

	now := time.Now()
	for _, info := range fileInfo {
		if diff := now.Sub(info.ModTime()); diff > cutoff {
			os.Remove(outputLocation + info.Name())
			fmt.Printf("Deleting %s which is %s old\n", info.Name(), diff)
		}
	}
}

func main() {
	if err := checkRequiredEnvVariables(); err != nil {
		fmt.Println(err.Error())
		os.Exit(1)
	}

	if err := setVariables(); err != nil {
		fmt.Println(err.Error())
		os.Exit(1)
	}

	run()

	c := cron.New()
	c.AddFunc(frequency, func() { run() })
	go c.Start()

	runStatusAPI()
}
