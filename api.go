package main

import (
	"fmt"
	"io"
	"log"
	"net/http"
	"os"

	"github.com/gorilla/mux"
)

func runStatusAPI() {
	fmt.Println(os.Stdout, "Starting API on 8000...\n")

	router := mux.NewRouter()
	router.HandleFunc("/", getStatus).Methods("GET")
	if err := http.ListenAndServe(":8000", router); err != nil {
		log.Println("API error: ", err.Error())
		os.Exit(1)
	}
}

func getStatus(w http.ResponseWriter, r *http.Request) {
	w.Header().Set("Content-Type", "application/json")
	w.WriteHeader(http.StatusOK)
	io.WriteString(w, `{"status": "alive"}`)
}
